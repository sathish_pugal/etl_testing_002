package com.cloudfire.arrayprograms;

import java.util.Scanner;

public class Program1_Ascending {

	public static void main(String[] args) {

		System.out.println("Enter the array size:  \n");
		Scanner sc = new Scanner(System.in);
		int size = sc.nextInt();
		int arr[] = new int[size];

		// int arr[] = new int[] {19,3,15,6,14,9};
		System.out.println("Enter the values for array: \n");

		for (int i = 0; i < arr.length; i++) {
			arr[i] = sc.nextInt();

		}
		sc.close();

		int temp = 0;

		for (int i = 0; i < arr.length; i++) {

			for (int j = i + 1; j < arr.length; j++) {

				if (arr[i] > arr[j]) {

					temp = arr[i];

					arr[i] = arr[j];

					arr[j] = temp;
				}

			}

			System.out.print(arr[i] + " ");

		}

	}

}


package com.cloudfire.patternprogram;

public class Pattern10_ABCPyramid2 {

	public static void main(String[] args) 
	{
		int alphabet = 65;
		int rows = 5;
		for (int i = rows; i >= 1; i--) 
		{
			for (int j = 1; j <= i; j++) 
			{
				System.out.print(" ");
			}
			for(int k = i; k <= rows; k++)
			{
				System.out.print((char)(alphabet + k - 1)+ " ");
			}
			System.out.println();
		}

	}

}





package com.cloudfire.interfaces;

public class InterfaceMain implements ChildInterface{

	public static void main(String[] args) 
	{
		ChildInterface ci = new InterfaceMain();
		ci.childDefault();
		ChildInterface.childDisplay();
		ci.show();
		ci.display();
		ci.childShow();
		System.out.println(ci.x);
		
		

	}

	@Override
	public void show() {
		System.out.println("This is from ParentInterface");
		
	}

	@Override
	public void display() {
		System.out.println("This is from ParentInterface");
		
	}

	@Override
	public void childShow() {
		System.out.println("This is from ChildInterface");
		
	}

}

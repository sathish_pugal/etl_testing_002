package com.cloudfire.interfaces;

public interface ChildInterface extends ParentInterface
{
	int x = 3000;
	public void childShow();

	public static void childDisplay()
	{
		System.out.println("This is static method from child interface");
	}
	public default void childDefault()
	{
		System.out.println("This is default method from child interface");	
	}
}

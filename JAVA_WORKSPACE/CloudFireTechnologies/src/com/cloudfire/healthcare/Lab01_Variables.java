package com.cloudfire.healthcare;

public class Lab01_Variables 
{
	int price = 5000;
	String str = "Kannan";
	float cost = 100.50f;
	static int salary = 10000;

	public static void main(String[] args)
	{
		/*
		int price = 5000;
		String str = "Kannan";
		float cost = 100.50f;
		 */
		Lab01_Variables obj = new Lab01_Variables();

		System.out.println("Hello");
		System.out.println(obj.price);
		System.out.println(obj.str);
		System.out.println(obj.cost);

		System.out.println(obj.add());
		double sal = Lab01_Variables.salary_hike();
		System.out.println(sal);

	}

	static 
	{

		System.out.println("static block");
	}
	public int add()
	{	int a;
		int b;
		int c;

		a = 10;
		b = 20;
		c = 30;

		c = (a+b);
		return c;
		
	}
	
	static double salary_hike()
	{
		double c = salary*(.90);
		return c;
	}

}

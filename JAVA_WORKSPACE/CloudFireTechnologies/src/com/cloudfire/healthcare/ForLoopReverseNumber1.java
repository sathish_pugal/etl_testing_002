package com.cloudfire.healthcare;

public class ForLoopReverseNumber1 {

	public static void main(String[] args) {
		int number = 123456789;
		int remainder;
		int reverse = 0;
		
		for(; number!=0; number = number/10)
		{
			remainder = number % 10;
			reverse = reverse * 10 + remainder;
		}
			System.out.println("Reverse number outer: "+reverse);
	}

}

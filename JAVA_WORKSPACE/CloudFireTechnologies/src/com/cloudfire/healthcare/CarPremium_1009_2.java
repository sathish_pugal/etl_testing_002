package com.cloudfire.healthcare;

public class CarPremium_1009_2 {
	
	
	public static void main(String[] args) {
		int mycar_premium = 200000;
		int year1 = 2018;
		double finalvalue = netPremium(year1,mycar_premium);
		System.out.println("10% Value: "+finalvalue);

	}
	public static double netPremium(int year,int premium1) {
		double netvalue = 0.0d;
		double mycar_premium1 = premium1;
		double percentage = 0.10d;
		if(year==2016) {
			netvalue = mycar_premium1 * percentage ;
			mycar_premium1 = mycar_premium1-netvalue;
		}else if(year==2017) {
			percentage = 0.20d;
			netvalue = mycar_premium1 * percentage ;
			mycar_premium1 = mycar_premium1-netvalue;
		}else if(year==2018) {
			percentage = 0.30d;
			netvalue = mycar_premium1 * percentage ;
			mycar_premium1 = mycar_premium1-netvalue;
		}
		System.out.println("Mycar Premium-10%: "+mycar_premium1);
		return netvalue;
	}
}

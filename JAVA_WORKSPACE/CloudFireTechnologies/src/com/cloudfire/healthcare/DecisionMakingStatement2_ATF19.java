package com.cloudfire.healthcare;

public class DecisionMakingStatement2_ATF19 {

	public static void main(String[] args) {
		int marks = 91;

		if((marks>=35 && marks<=40))
		{
			System.out.println("Student Passed: & Grade D");
		}
		else if((marks>40 && marks<=50))
		{
			System.out.println("Student Passed: & Grade C");
		}
		else if((marks>50 && marks<=70))
		{
			System.out.println("Student Passed: & Grade B");
		}
		else if((marks>70) && marks<=90)
		{
			System.out.println("Student Passed: & Grade A");
		}
		else if((marks>90 && marks<=100))
		{
			System.out.println("Student Passed: & Grade A++");
		}
		else
		{
			System.out.println("Student Failed:");
		}

	}

}

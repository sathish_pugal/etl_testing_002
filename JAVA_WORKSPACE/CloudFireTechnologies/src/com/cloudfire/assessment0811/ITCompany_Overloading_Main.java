package com.cloudfire.assessment0811;

public class ITCompany_Overloading_Main {

	public static void main(String[] args) {
		ITCompany_Overloading it = new ITCompany_Overloading();
		it.juniordeveloper("DOTNET", "SCALA");
		it.juniordeveloper("JAVA", "GROOVY", "C");
		it.juniordeveloper("JAVA", "GROOVY", "C","PYTHON");
		it.juniordeveloper("JAVA", "GROOVY", "C","PYTHON", "SOLIDITY");
		System.out.println();
		it.seniordeveloper("DOTNET", "SCALA");
		it.seniordeveloper("JAVA", "GROOVY", "C");
		it.seniordeveloper("JAVA", "GROOVY", "C","PYTHON");
		it.seniordeveloper("JAVA", "GROOVY", "C","PYTHON", "SOLIDITY");


	}

}

package com.cloudfire.assessment0811;

public class ITCompany_Overloading {

	/*
	 * 3.
Implement the below scenario
For example in a IT company there are any developers some categories may be junior developer , senior developer , project manager and etc.

There are many junior developers and may senior developers.

As the category is same as a junior developer one is for .net & Scala, one is for java & groovy , one is for python & solidity etc..

So all these junior developers of different languages are can be called as overloaded and similarly all the senior developers of different language are overloaded.

	 */
	String dotnet;
	String scala;
	String java;
	String groovy;
	String c;
	String python;
	String solidity;
	
	
	protected void juniordeveloper(String dotnet, String scala)
	{
		System.out.println("Junior Developer: "+dotnet+ " & "+scala);
		
	}
	protected void juniordeveloper(String java, String groovy,String c)
	{
		System.out.println("Junior Developer: "+java+ " & "+groovy+ " & "+c);
		
	}
	protected void juniordeveloper(String java, String groovy,String c,String python)
	{
		System.out.println("Junior Developer: "+java+ " & "+groovy+ " & "+c+" & "+python);
		
	}
	protected void juniordeveloper(String java, String groovy,String c,String python,String solidity)
	{
		System.out.println("Junior Developer: "+java+ " & "+groovy+ " & "+c+" & "+python+ " & "+solidity);
		
	}
	
	
	
	protected void seniordeveloper(String dotnet, String scala)
	{
		System.out.println("Senior Developer: "+dotnet+" & "+scala);
		
	}
	protected void seniordeveloper(String java, String groovy,String c)
	{
		System.out.println("Senior Developer: "+java+ " & "+groovy+ " & "+c);
		
	}
	protected void seniordeveloper(String java, String groovy,String c,String python)
	{
		System.out.println("Senior Developer: "+java+ " & "+groovy+ " & "+c+" & "+python);
		
	}
	protected void seniordeveloper(String java, String groovy,String c,String python,String solidity)
	{
		System.out.println("Senior Developer: "+java+" & "+groovy+ " & "+c+" & "+python+" & "+solidity);
		
	}


}

package com.cloudfire.constructor;

public class Parameterised_Constructor {
	
	int emp_id;
	String emp_name;
	
	protected Parameterised_Constructor(int emp_id,	String emp_name)
	{
		this.emp_id = emp_id;
		this.emp_name = emp_name;
		
		System.out.println(emp_id+" "+emp_name);
	}

	
	void display()
	{
		System.out.println("It is from Display method-----"+"EmpID: "+ emp_id+"  EmpName: "+emp_name);
	}
}

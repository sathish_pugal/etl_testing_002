package com.cloudfire.constructor;

public class Cons_Overloading {
	int voterid;
	String votername;
	int age;
	String gender;
	
	
	Cons_Overloading(int voterid,String votername,int age)
	{
		this.voterid = voterid;
		this.votername = votername;
		this.age = age;
	}
	
	Cons_Overloading(int voterid,String votername,int age,String gender )
	{
		//Constructor overloading & Constructor Chaining
		this(voterid,votername,age);
		this.gender = gender;
		System.out.println("2nd Constructor:  "+"VoterID: "+voterid+"| Votername: "+votername+"| Age: "+age+"| Gender: "+gender);

	}
	
	Cons_Overloading(Cons_Overloading c )
	{
		//Copy Constructor
		this(c.voterid,c.votername,c.age);
		this.gender = c.gender;

	}


	void display()
	{
		System.out.println("It is from display method:- VoterID"
				+ ":  "+voterid+"| Votername: "+votername+"| Age: "+age+"| Gender: "+gender);
	}

}

package com.cloudfire.constructor;

public class Cons_Overloading_Main {

	public static void main(String[] args) {

		new Cons_Overloading(123,"Ram",30).display();
		Cons_Overloading co = new Cons_Overloading(12345,"Kumaran",30,"Female");
		co.display();
		Cons_Overloading obj = new Cons_Overloading(co);
		co.display();
	}

}

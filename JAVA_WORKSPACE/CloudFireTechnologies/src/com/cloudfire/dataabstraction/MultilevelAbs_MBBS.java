package com.cloudfire.dataabstraction;

public abstract class MultilevelAbs_MBBS {
	
	//Instance Variable
	int neetmarks;
	String studentname;
	int rank;
	
	protected MultilevelAbs_MBBS(int neetmarks,String studentname, int rank)
	{
		this.neetmarks=neetmarks;
		this.studentname=studentname;
		this.rank=rank;
		
	}
	
	protected abstract int neetMarkCutOff();
	
	protected abstract int rank();
	
	protected int eligibleForScholarship(int neetmarks)
	{
		
		if(neetmarks>=720)
			System.out.println("Eligible for 100% Scholarship");
		else if(neetmarks<720 && neetmarks>=600 )
			System.out.println("Eligible for 50% Scholarship");
		else
			System.out.println("Not Eligible");
		
		return neetmarks;
		
		
	}
	
	@Override
	public String toString() {
		return "MultilevelAbs_MBBS [neetmarks=" + neetmarks + ", studentname=" + studentname + ", rank=" + rank + "]";
	}
	
	

}

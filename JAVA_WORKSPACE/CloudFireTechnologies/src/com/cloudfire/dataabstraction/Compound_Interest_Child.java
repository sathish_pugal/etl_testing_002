package com.cloudfire.dataabstraction;

public class Compound_Interest_Child extends CompoundInterest {


	public static void main(String[] args) {

		CompoundInterest ci = new Compound_Interest_Child();
		ci.calculate_compoundInt(3000.0,6.0,1.0,1.0);
		ci.display(3000.0,6.0,1.0,1.0);
	}

	@Override
	public void calculate_compoundInt(double p,double r,double n, double t) {


		amount = p * Math.pow(1 + (r/n),n * t);
		compoundint = amount - p;

	}

}

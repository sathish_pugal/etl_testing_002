package com.cloudfire.dataabstraction;

public class DataAbstraction_Example1_Main extends DataAbstraction_Example1 {

	public static void main(String[] args) {
		
		//DataAbstraction_Example1 de = new DataAbstraction_Example1();
		// We can not instantiate abstract class DataAbstraction_Example1
		
		DataAbstraction_Example1 de = new DataAbstraction_Example1_Main();
		System.out.println(de.hospital());
		de.nonabstract_method();
		

	}

	@Override
	public int hospital() {
		
		return 999999990;
	}

}

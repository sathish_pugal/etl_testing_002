package com.cloudfire.scripts;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class TS05_ReadData_ColumnName2 {

		public File file = null;
		public FileInputStream fis = null;
		public static BufferedInputStream buf = null;
		public static XSSFWorkbook workbook = null;
		public static XSSFSheet sheet = null;
		public static XSSFRow row = null;
		public static XSSFCell cell = null;
		
		public TS05_ReadData_ColumnName2(String filepath) throws FileNotFoundException
		{
			file = new File(filepath);
			fis = new FileInputStream(file);
			buf = new BufferedInputStream(fis);
			
		}
		
		public static void main(String[] args) throws FileNotFoundException {
			String filepath = System.getProperty("user.dir")+File.separator+"src\\test\\resources\\testData\\TestData_1.xlsx";
			TS05_ReadData_ColumnName2 obj = new TS05_ReadData_ColumnName2(filepath);
			int col_num = -1;
			try {
				workbook = new XSSFWorkbook(buf);
				sheet = workbook.getSheet("Credentials");
				row = sheet.getRow(0);
				for(int col_index=0;col_index < row.getLastCellNum();col_index++) {
				if(row.getCell(col_index).getStringCellValue().trim().equalsIgnoreCase("NoOfAttempts"))
				{
					col_num = col_index;
				}
				}
				row = sheet.getRow(1);
				double value = 0.0d;
				if(row!=null)
				{
					cell = row.getCell(col_num);
				}
				if(cell!=null)
				{
					value = cell.getNumericCellValue();
				}
				System.out.println(value );
				
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

}

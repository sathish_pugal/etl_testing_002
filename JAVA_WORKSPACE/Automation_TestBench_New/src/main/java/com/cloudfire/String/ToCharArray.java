package com.cloudfire.String;

public class ToCharArray {

	public static void main(String[] args) {
		String name = "Kannan";
		char ch[] =    name.toCharArray();

		for (char c : ch) {
			System.out.print(c);
		}

		System.out.println("");
		int count = 0;
		for (int i = 0; i < ch.length; i++) {
			System.out.print(ch[i]);
			if(ch[i]=='n')
				count++;

		}
		System.out.println("");
		System.out.println(count);
	}

}

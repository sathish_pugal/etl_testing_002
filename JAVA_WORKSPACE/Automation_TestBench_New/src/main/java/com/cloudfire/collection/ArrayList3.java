package com.cloudfire.collection;

import java.util.ArrayList;
import java.util.List;



public class ArrayList3 {

	public static void main(String[] args) {
		
		 List<String> al = new ArrayList<String>(12);
		 System.out.println(al.size());
		 
		 al.add(0, "mango");
		 al.add(1,"banana");
		 al.add(2,"grapes");
		 al.add(3,"orange");
		 
		 System.out.println(al.size());
		 al.remove(0);
		 System.out.println(al);
		 al.set(0, "kannan");
		 al.remove("grapes");
		 System.out.println(al);
		 al.removeAll(al);
		 System.out.println(al);
		 System.out.println(al.isEmpty());
		 al.toString();
		 System.out.println(al);
	}

}

package com.cloudfire.trycatch;

public class TryCatchExample1 {

	public static void main(String[] args) {

		try {

			int arr[] = new int[5];
			//arr[5] = 30/0;
			//arr[5] = 30;
			arr[5] = 30/0;
			System.out.println(arr[12]);

		}
		
		catch (ArrayIndexOutOfBoundsException e) {
			e.printStackTrace();
		}

		catch (ArithmeticException e) {
			e.printStackTrace();
			System.out.println("--"+e);
		}
		catch (Exception e) {
			e.printStackTrace();
		}

		System.out.println("oooooooo");

	}

}

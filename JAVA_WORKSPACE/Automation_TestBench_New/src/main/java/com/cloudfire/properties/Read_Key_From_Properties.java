package com.cloudfire.properties;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Properties;

public class Read_Key_From_Properties {
	
	public static String readData_Property(String key,String filename)
	{
		FileReader file = null ;
		try {
			file = new FileReader(System.getProperty("user.dir")+File.separator+"src\\main\\resources\\properties"+File.separator+filename+".properties");
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		Properties prop = new Properties();
		try {
			prop.load(file);
		} catch (IOException e) {
			e.printStackTrace();
		}

		return prop.getProperty(key);
	}

	public static void main(String[] args) {
		System.out.println(readData_Property("username","login"));
				
	}

}

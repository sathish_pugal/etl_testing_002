package com.cloudfire.arrays;

public class Arrays_Instantiation {

	public static void main(String[] args) {
		int arr[];
		arr = new int[5];
		
		arr[0]=3;
		arr[1]=6;
		arr[2]=9;
		arr[3]=10;
		arr[4]=11;
		
		
		for (int i = 0; i < arr.length; i++) {
			System.out.println(arr[i]);
		}
		System.out.println("");
		
		int num[] = new int[10];
		
		for (int i = 0; i < num.length; i++) {
			num[i] = i+1;
			
		}
		for (int i = 0; i < num.length; i++) {
			System.out.println("Array values: "+num[i]);
		}
		System.out.println("");
		for (int i : num) {
			System.out.println(i);
		}
	}
	

}

package com.cloudfire.testbench;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;

import com.aventstack.extentreports.Status;
import com.cloudfire.*;

import com.config.utils.Util;

public class ZoomIN_ZoomOut extends Util {

	public static void main(String[] args) throws InterruptedException {
		Util.browserName("GC", "window.location='https://opensource-demo.orangehrmlive.com/web/index.php/auth/login'");
		Util.orangeHRMLogin();
		Util.setExtent();
		Util.zoomIn_JS();
		Util.zoomOut_JS();
		Util.zoom_100_Percent();
		Util.endReport();

	}

}

package com.cloudfire.testbench;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.ElementNotVisibleException;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;

import com.cloudfire.config.Configuration;

public class TC63_Enabling_Disabling_TextBox {

	public static void main(String[] args) throws InterruptedException {
		System.setProperty("webdriver.chrome.driver",
				Configuration.userdir + Configuration.driver_path + "chromedriver.exe");
		WebDriver driver = new ChromeDriver();
		driver.get(Configuration.url);
		driver.manage().window().maximize();
		By uname = By.name("username");
		FluentWait<WebDriver> wait = new FluentWait<WebDriver>(driver).withTimeout(30, TimeUnit.SECONDS)
				.pollingEvery(5, TimeUnit.SECONDS).ignoring(NoSuchElementException.class)
				.ignoring(ElementNotVisibleException.class);
		WebElement user_name = wait.until(ExpectedConditions.presenceOfElementLocated(uname));
		JavascriptExecutor js = (JavascriptExecutor) driver;
		//To Disable the text field		
		String toDisable = "arguments[0].setAttribute('disabled','')";
		js.executeScript(toDisable, user_name);
		boolean bval = user_name.isEnabled();
		System.out.println("The username is disabled:  " + bval);
		
		  if(bval==false) 
			  System.out.println("Username text field is disabled"); 
		  else
		  System.out.println("Username text field is enabled");
		  //To Enable the text field
		  String toEnable = "arguments[0].removeAttribute('disabled')";
		 // Thread.sleep(5000);
		  js.executeScript(toEnable, user_name);
		  boolean bval_toEnable = user_name.isEnabled();
		  System.out.println("The usename text field is enabled: "+ bval_toEnable);
		  if(bval_toEnable == true)
			  System.out.println("The username text field is Enabled");
		  else
			  System.out.println("The username text field is Disabled");
		  
		 
	}
}

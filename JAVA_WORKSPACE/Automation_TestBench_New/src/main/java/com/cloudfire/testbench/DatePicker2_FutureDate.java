package com.cloudfire.testbench;


import java.io.File;
import java.util.List;
import java.util.Properties;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.cloudfire.config.Configuration;
import com.config.utils.Reports;
import com.config.utils.Util;

public class DatePicker2_FutureDate extends Util {
	
	   static WebDriver driver;

		public static void main(String[] args) throws InterruptedException {
			
		Reports report = new Reports(driver);
		report.takeScreenShot(Configuration.userdir+File.separator+"screenshots"+File.separator, "login_page1.png");
		report.takeScreenShot_WithTimeStamp(Configuration.userdir+File.separator+"screenshots"+File.separator,".png");
		Util.setExtent();
		datePickerFuture("25","October 2023");
		report.takeScreenShot(Configuration.userdir+File.separator+"screenshots"+File.separator, "login_page2.png");
		report.takeScreenShot_WithTimeStamp(Configuration.userdir+File.separator+"screenshots"+File.separator,".png");
		Util.endReport(); 
		report.takeScreenShot(Configuration.userdir+File.separator+"screenshots"+File.separator, "login_page3.png");
		report.takeScreenShot_WithTimeStamp(Configuration.userdir+File.separator+"screenshots"+File.separator,".png");
		//Util.endSession();
		}
}
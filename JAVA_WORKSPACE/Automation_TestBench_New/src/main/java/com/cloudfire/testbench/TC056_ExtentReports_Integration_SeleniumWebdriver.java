package com.cloudfire.testbench;

import java.io.File;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import com.cloudfire.config.Configuration;
import com.config.utils.Reports;
import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

public class TC056_ExtentReports_Integration_SeleniumWebdriver {
	public static WebDriver driver;
	 public static ExtentReports extent;
	 public static  ExtentTest test;
	 //Reports report = new Reports(driver );

	public static void main(String[] args) throws InterruptedException {
		extent = new ExtentReports(System.getProperty("user.dir")+File.separator+"Reports"+File.separator+"ExtentReport.html", false);
		extent.loadConfig(new File(System.getProperty("user.dir")+File.separator+"src\\main\\resources\\config\\extent_config.xml"));
		//Pre-conditions
		test = extent.startTest("Build Verification");
		test.assignAuthor("Kannan");
		test.assignCategory("SmokeReports - Production");
		System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir")+File.separator+Configuration.driver_path+File.separator+"chromedriver.exe");
		driver = new ChromeDriver();
		driver.navigate().to(Configuration.url);
		test.log(LogStatus.PASS, "Browser launched successfully");
		driver.manage().timeouts().implicitlyWait(5000, TimeUnit.SECONDS);
		driver.manage().window().maximize();
		Reports report = new Reports(driver );
		String expected_url = Configuration.url;
		//String expected_url = "https://opensource-demo.orangehrmlive.com1/web/index.php/auth/login1";
		String actual_url = driver.getCurrentUrl();
		test.log(LogStatus.PASS, "The current url of the webpage is::::::"+actual_url);
		if(expected_url.equals(actual_url))
		{
			test.log(LogStatus.PASS,"Actual URL::::::"+actual_url);
			test.log(LogStatus.PASS, "URL is correct");
		}
		else
		{
			test.log(LogStatus.FAIL, "Actual URL: "+actual_url + "Expected URL :"+expected_url);
			Thread.sleep(7000);
			test.log(LogStatus.FAIL, test.addScreenCapture(report.takeScreenShot_Failures_WithTimeStamp(System.getProperty("user.dir")+File.separator+"screenshots"+File.separator, "png")));
		}
		//Post-conditions
		extent.endTest(test);
		driver.close();
		test.log(LogStatus.PASS, "Browser closed successfully");
		extent.flush();
		extent.close();
		
		 	
		 

	}

}

package com.cloudfire.collection;

import java.util.ArrayDeque;
import java.util.Deque;
import java.util.Iterator;
import java.util.PriorityQueue;

public class DeQueueExample {

	public static void main(String[] args) {
		Deque<String> pq = new ArrayDeque<String>();
		pq.add("Java");
		pq.add("Python");
		pq.add("Ruby");
		pq.add("C#");
		pq.add("C");
		pq.add("C");
		pq.add("C");
		pq.add("C");
		// pq.add(null);
		pq.remove("C");
		pq.add("Ruby");
		pq.remove("Ruby");
		pq.poll();
		pq.poll();
		pq.element();
		pq.element();
		pq.peek();
		pq.remove();
		System.out.println(pq);
		pq.peek();
		System.out.println(pq);
		Iterator<String>itr = pq.iterator();
		while(itr.hasNext())
		{
			System.out.println(itr.next());
		}
		pq.remove();
		pq.poll();
		Iterator<String>itr2 = pq.iterator();
		while(itr2.hasNext())
		{
			System.out.println(itr2.next());
		}
		pq.stream().filter(x->x.startsWith("R")).forEach(System.out::println);
		

	}

}

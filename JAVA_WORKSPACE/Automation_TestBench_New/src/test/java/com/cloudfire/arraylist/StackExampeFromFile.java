package com.cloudfire.arraylist;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Stack;


public class StackExampeFromFile {
	
	static String filepath = System.getProperty("user.dir") + File.separator + "src\\test\\resources\\Stack.txt";

	public Stack<String> stackMethod(String filepath) throws IOException {
		File file = null;
		FileReader fr = null;
		BufferedReader br = null;
		String line = null;
		Stack<String> al = new Stack<String>();

		
		try {
			file = new File(filepath);
			fr = new FileReader(file);
			br = new BufferedReader(fr);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		
		try {
			while ((line = br.readLine())!= null) {
				//al.add(line);
				al.push(line);
			}
			return al;
		} finally {
			try {
				br.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		
	}

	public static void main(String[] args) throws IOException {

		StackExampeFromFile obj = new StackExampeFromFile();
		System.out.println(obj.stackMethod(filepath));

	}

}

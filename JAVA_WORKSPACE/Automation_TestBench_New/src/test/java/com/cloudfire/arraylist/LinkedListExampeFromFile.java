package com.cloudfire.arraylist;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.LinkedList;

public class LinkedListExampeFromFile {
	
	static String filepath = System.getProperty("user.dir") + File.separator + "src\\test\\resources\\LinkedList.txt";

	public LinkedList<String> linkedList(String filepath) throws IOException {
		File file = null;
		FileReader fr = null;
		BufferedReader br = null;
		String line = null;
		LinkedList<String> al = new LinkedList<String>();

		
		try {
			file = new File(filepath);
			fr = new FileReader(file);
			br = new BufferedReader(fr);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		
		try {
			while ((line = br.readLine())!= null) {
				al.add(line);
			}
			return al;
		} finally {
			try {
				br.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		
	}

	public static void main(String[] args) throws IOException {

		LinkedListExampeFromFile obj = new LinkedListExampeFromFile();
		System.out.println(obj.linkedList(filepath));

	}

}

package com.cloudfire.browserstack;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.ElementClickInterceptedException;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;


public class BrowserStackSampleTest {

	
    //Username: cloudfire_hfO5by
	//Access_Key: 8ieMVfWxqJa4pbKVcXmL



public static final String USERNAME = "cloudfire_hfO5by";
public static final String AUTOMATE_KEY = "8ieMVfWxqJa4pbKVcXmL";
public static final String URL = "https://" + USERNAME + ":" + AUTOMATE_KEY + "@hub-cloud.browserstack.com/wd/hub";
public static void main(String[] args) throws Exception {
boolean monthYearnotfound;
DesiredCapabilities caps = new DesiredCapabilities();

caps.setCapability("os", "Windows");
caps.setCapability("os_version", "10");
caps.setCapability("browser", "firefox");
caps.setCapability("browser_version", "104");
//caps.setCapability("browser_version", "102");

caps.setCapability("name", "First Test_Kannan");

WebDriver driver = new RemoteWebDriver(new java.net.URL(URL), caps);
JavascriptExecutor js =(JavascriptExecutor)driver;

js.executeScript("window.location='https://www.goibibo.com/flights/'");
driver.manage().window().maximize();

driver.findElement(By.xpath("(//span[text()='Departure']//..)[1]")).click();

String month_year="May 2023";
String date="30";

monthYearnotfound=true;


outer:
while (monthYearnotfound) {


List<WebElement>  headings       = driver.findElements(By.xpath("//div[@class='DayPicker']//div[@role='heading']"));

if ((!headings.get(0).getText().trim().equalsIgnoreCase(month_year)) && (!headings.get(1).getText().trim().equalsIgnoreCase(month_year))){
driver.findElement(By.xpath("//div[@class='DayPicker-NavBar']/span[2]")).click();
}else {
//monthYearnotfound=false;
break outer;

}


}

List<WebElement>  date_lst     = driver.findElements(By.xpath("//div[@class='DayPicker']//div[@role='heading']/div[text()='"+month_year+"']//..//..//div[@class='DayPicker-Body']/div/div/p"));


   WebElement       done_btn  = driver.findElement(By.xpath("//span[text()='Done']"));
   
   
WebElement        mon_yr  = driver.findElement(By.xpath("//div[@class='DayPicker']//div[@role='heading']/div[text()='"+month_year+"']"));


for (WebElement cell : date_lst) {
if (cell.getText().trim().equals(date) && (mon_yr.getText().trim().equalsIgnoreCase(month_year))) {

try {
cell.click();
} catch (ElementClickInterceptedException e) {
e.printStackTrace();
}catch (StaleElementReferenceException e) {
e.printStackTrace();
}catch (Exception e) {
e.printStackTrace();
}
done_btn.click();
break;
}

}
}

}

package com.cloudfire.String;

public class StringAssign8 {

	public static void main(String[] args) {

		//8. find the index of the given word: "is","index",("is",8),("is",4) for a given string 
			
		String s1="this is index of example";
		
		System.out.println(s1.indexOf("is"));
		System.out.println(s1.indexOf("index"));
		System.out.println(s1.indexOf("is", 8));
		System.out.println(s1.indexOf("is", 4));
			
	}
}


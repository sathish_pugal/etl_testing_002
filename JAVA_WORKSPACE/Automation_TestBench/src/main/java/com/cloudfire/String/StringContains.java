package com.cloudfire.String;

public class StringContains {

	public static void main(String[] args) {
		String name = "I am Kannan";
		System.out.println(name.contains("am"));
		System.out.println(name.contains(" "));
		System.out.println(name.contains("Kannan"));
		
		//boolean b = name.contains(null);
		boolean bool = name.contains("I");
		System.out.println(bool);
		
		if(name.contains("K"))
		{System.out.println("Valid");}
		else
		{
			System.out.println("Invalid");
		}
		
		

	}

}

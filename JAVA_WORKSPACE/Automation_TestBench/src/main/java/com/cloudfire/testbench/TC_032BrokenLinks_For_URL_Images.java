package com.cloudfire.testbench;
import com.config.utils.Util;

public class TC_032BrokenLinks_For_URL_Images {

	public static void main(String[] args) {
		Util.chromeBrowserLaunch("window.location='http://only-testing-blog.blogspot.com/2013/09/testing.html'");
		Util.brokenLinksURL();
		Util.brokenLinksImage();
	}
}



package com.cloudfire.testbench;

import java.net.MalformedURLException;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.config.utils.Util;

public class TC051_BrowserStack_Integration extends Util {

	public static final String USERNAME="kannanvelappan_LM6Xv2";
	public static final String ACCESS_KEY="mYsm3HAyXCpqJMBBGUwV";
	public static final String URL = "https://"+USERNAME+":"+ACCESS_KEY+"@hub-cloud.browserstack.com/wd/hub";
	public static void main(String[] args) throws InterruptedException {
		DesiredCapabilities caps = new DesiredCapabilities();
		caps.setCapability("os","Windows");
		caps.setCapability("os_version","10");
		caps.setCapability("browser","chrome");
		caps.setCapability("browser_version","108");
		caps.setCapability("name", "Date Picker Test Case");

		try {
			driver = new RemoteWebDriver(new java.net.URL(URL), caps);
		} catch (MalformedURLException e) {
			e.printStackTrace();
		}

		//int max_height = driver.manage().window().getSize().getHeight();
		//int max_width = driver.manage().window().getSize().getWidth();
		driver.get("http://only-testing-blog.blogspot.com/2014/09/selectable.html");
		WebElement date = driver.findElement(By.xpath("//input[@id='datepicker' or @type = 'text']"));
		date.click();
		Util.setExtent();
		Util.datePicker("12");
		Util.endReport();

	}

}

package com.cloudfire.array;

import java.util.HashSet;

public class Uncommon_Elements {
	
	public static String uncommon(String s1[],String s2[])
	{
		String s11="";
		HashSet<String> hs = new HashSet<String>();

		for (int i = 0; i < s1.length; i++) {
			for (int j = 0; j < s2.length; j++) {
				if (s1[i] != s2[j]) {
					hs.add(s1[i]);
				}
			}
		}

		s11=s11+hs;
		return s11;
	}
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		String s1[] = { "ONE", "TWO", "FOUR", "FIVE" };
		String s2[] = { "SIX", "SEVEN", "ONE", "TWO" };
		System.out.println(uncommon(s1,s2));
		
	}

}


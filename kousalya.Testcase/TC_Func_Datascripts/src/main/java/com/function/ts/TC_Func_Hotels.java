package com.function.ts;

import java.time.Duration;
import java.util.List;

import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

public class TC_Func_Hotels {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Logger log=LogManager.getLogger(TC_Func_Location.class);
		BasicConfigurator.configure();
		System.setProperty("webdriver.edge.driver",System.getProperty("user.dir") +"\\driver\\msedgedriver.exe");
		WebDriver driver=new EdgeDriver();
		  driver.manage().window().maximize();
		  driver.get("https://adactinhotelapp.com/");
		  WebDriverWait wait=new WebDriverWait(driver, Duration.ofMillis(30000));
          WebElement user= wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//input[@name='username']")));
          user.sendKeys("kousalya97");
		  WebElement pass = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//input[@name='password']")));
		  pass.sendKeys("kousalya");
		  WebElement login = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//input[@name='login']")));
		  login.click();
		  String[] str={"- Select Hotel -","Hotel Creek","Hotel Sunshine","Hotel Hervey","Hotel Cornice"};
		  Select select =new Select(driver.findElement(By.xpath("//select[@name='hotels']")));
		  List<WebElement> lis=select.getOptions();
		  for (int i = 0; i < str.length; i++) {
			//  System.out.println("the elements from hotels::::"+    lis.get(i).getText());
			//  System.out.println("the elements from string array:::"+str[i] );
			  if(str[i].equalsIgnoreCase(lis.get(i).getText())) {
				  lis.get(i).click();
				  log.info("Testcase Pass");
			  }else {
				  log.info("Testcase Fail");
			  }
			
		}
		  //for (WebElement options : lis) {
			//  log.info(options.getText());
			 
	
               driver.quit();
}
}
package com.smoke.tc_01;

import java.io.FileInputStream;

import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class TC_Read_Column1 {

		 public static void main(String args[]) throws Exception
		    {
		        FileInputStream fis = new FileInputStream("C:\\excel\\Book1.xlsx");
		        XSSFWorkbook workbook = new XSSFWorkbook(fis);
		        XSSFSheet sheet = workbook.getSheet("Sheet2");
		        XSSFRow row = sheet.getRow(1);
		        XSSFCell cell = row.getCell(0);
		 
		        String userName = String.valueOf(cell.getNumericCellValue());
		        System.out.println("Value from the Excel sheet :"+ userName);
		    }
		

	}



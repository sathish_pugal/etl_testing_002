SELECT * FROM TABLE(VALIDATE(employee_tbl, JOB_ID=>'01b2962e-3201-0126-0000-000802eea135'));
select * from snowflake.account_usage.query_history;




// Day04 How to create a Snowflake Virtual Warehouse  Create a Warehouse with Object Properties
CREATE OR REPLACE RESOURCE MONITOR SMS_WAREHOUSE_MONITOR
  WITH CREDIT_QUOTA = 5000
  TRIGGERS ON 75 PERCENT DO NOTIFY
           ON 100 PERCENT DO SUSPEND
           ON 110 PERCENT DO SUSPEND_IMMEDIATE;

CREATE WAREHOUSE if not exists sms_warehouse
WAREHOUSE_TYPE = 'snowpark-optimized'
WAREHOUSE_SIZE = LARGE
MAX_CLUSTER_COUNT = 4
MIN_CLUSTER_COUNT = 1
SCALING_POLICY =  ECONOMY
AUTO_SUSPEND = 200
AUTO_RESUME = TRUE
INITIALLY_SUSPENDED = TRUE
RESOURCE_MONITOR = 'SMS_WAREHOUSE_MONITOR'
COMMENT = 'SMS WARE HOUSE IS STARTED'
ENABLE_QUERY_ACCELERATION = TRUE
QUERY_ACCELERATION_MAX_SCALE_FACTOR = 2;

/* MAX_CONCURRENCY_LEVEL = 10 
  STATEMENT_QUEUED_TIMEOUT_IN_SECONDS = 60  
  STATEMENT_TIMEOUT_IN_SECONDS = 3600 
*/

SHOW WAREHOUSES; 
DESC WAREHOUSE sms_warehouse;
DESCRIBE WAREHOUSE sms_warehouse;

// Day05 Creation of Snowflake Database Schema and Tables Table Structure and Micro partitions

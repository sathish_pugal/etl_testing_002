package com.function.testing;

import java.time.Duration;
import java.util.List;

import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;



public class TC_Futuredate_01 {
	      public static void datepic(String dateval) {
	      Logger log=LogManager.getLogger(TC_Futuredate_01.class);
	  	  BasicConfigurator.configure();
	      System.setProperty("webdriver.edge.driver",System.getProperty("user.dir") +"\\driver\\msedgedriver.exe");
		  WebDriver driver=new EdgeDriver();
		  driver.manage().window().maximize();
		  driver.get("https://www.path2usa.com/travel-companion/");
		  JavascriptExecutor js=(JavascriptExecutor)driver;
		 
		  WebDriverWait wait=new WebDriverWait(driver,Duration.ofMillis(30000));
          WebElement from= wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//input[@id='form-field-travel_from']")));
          from.sendKeys("Los Angeles International Airport - (LAX) - Los Angeles, CALIFORNIA, USA");
	      WebElement to = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//input[@id='form-field-travel_to']")));
	      to.sendKeys("Chennai International Airport - (MAA) - Chennai, Tamil Nadu, IN");
	      WebElement dateform = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//input[@id='form-field-travel_comp_date']")));
	      js.executeScript("arguments[0].scrollIntoView();",dateform);   
	      dateform.click();
	      while(driver.findElement(By.xpath("//span[@class='cur-month']")).getText().contains("November"));
	      {
	    	  
	    	  driver.findElement(By.xpath("//span[@class='flatpickr-next-month']")).click();
	    	  
	      }
	      
	           List<WebElement> lis = driver.findElements(By.xpath("//div[@class='flatpickr-innerContainer']/div[1]/div[2]/div[1]/span"));
	           for(WebElement element:lis) {
	        	   if(element.isEnabled()) {
	    	   try {
	    	   String date = element.getText();
	    	   if (date.equalsIgnoreCase(dateval)) {
	    		   element.click();
	    	   }
	    	   }
	    	   catch(StaleElementReferenceException e) {
	    		   e.printStackTrace();
	    	  }}
	           else {
	    		   log.error("disabled");
	    	   }
	           }
               }		   
                public static void main(String[] args) {
		        try {
	            datepic("1");
		        }
		        catch(StaleElementReferenceException e) {
			    e.printStackTrace();
		        }
		        }
                }
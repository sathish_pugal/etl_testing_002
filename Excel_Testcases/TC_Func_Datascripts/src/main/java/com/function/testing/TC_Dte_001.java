package com.function.testing;

import java.time.Duration;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.ElementClickInterceptedException;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class TC_Dte_001 {
    public static void main(String[] args) {
		
	
    	System.setProperty("webdriver.edge.driver",System.getProperty("user.dir") +"\\driver\\msedgedriver.exe");
		  WebDriver driver=new EdgeDriver();
		  driver.manage().window().maximize();
		  driver.get("https://www.path2usa.com/travel-companion/");
		 // String Dateoftravel="December 2022";
		  String dateval="18";
		  
		  JavascriptExecutor js=(JavascriptExecutor)driver;
		 
		  WebDriverWait wait=new WebDriverWait(driver,Duration.ofMillis(30000));
          WebElement from= wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//input[@id='form-field-travel_from']")));
          from.sendKeys("Los Angeles International Airport - (LAX) - Los Angeles, CALIFORNIA, USA");
	      WebElement to = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//input[@id='form-field-travel_to']")));
	      to.sendKeys("Chennai International Airport - (MAA) - Chennai, Tamil Nadu, IN");
	      WebElement dateform = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//input[@id='form-field-travel_comp_date']")));
	      js.executeScript("arguments[0].scrollIntoView();",dateform);   
	      dateform.click();
	      while(true)
	    	{
	    	 String month=driver.findElement(By.xpath("//div[@class='flatpickr-month']")).getText();
	    	 if(month.equals("November 2022")) {
	    	 break;
	    	 }else {
	    	 driver.findElement(By.xpath("//span[@class='flatpickr-next-month']")).click();
	    	 } 
	      }
	      
	       List<WebElement> lis = driver.findElements(By.xpath("//div[@class='flatpickr-innerContainer']/div[1]/div[2]/div[1]/span"));
	       for(WebElement element:lis) {
	    	   String date = element.getText();
	    	   if (date.equals(dateval)) {
	    		   element.click();
	    		   break;
				
			}
	       }
	       }
    }
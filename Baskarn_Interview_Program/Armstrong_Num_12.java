package com.cloudfire.Interview_Program;

import java.util.Scanner;

public class Armstrong_Num_12 {

		
		public static boolean armstrong_num(int Number){
			
			String re="";
			int n = Number;
			int Sum = 0, r;

			while (n != 0) {
				r = n % 10;
				n = n / 10;
				Sum = Sum + r * r * r;
			}

			if (Number == Sum) {

				return true;

			}else {

				return false;
			}
		}

		public static void main(String[] args) {
		
			Scanner sc = new Scanner(System.in);
			System.out.println("Enter the Number to be checked");
			int Number = sc.nextInt();
			System.out.println(armstrong_num(Number));
			sc.close();

		}

	}

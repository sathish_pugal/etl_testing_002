package com.cloudfire.Interview_Program;

import org.junit.runner.JUnitCore;
import org.junit.runner.Result;
import org.junit.runner.notification.Failure;

                public class TestRunner {

	            public static void main(String[] args) {
		

				Result result = JUnitCore.runClasses(AscendingNum_01.class, DescendingNum_02.class, DuplicateNum_03.class, CommonNum_04.class, 
						Index_Position_05.class, MaximumNum_06.class, MinimumNum_07.class, Leap_Year_08.class, Pair_Num_09.class, HarshadNum_10.class, 
						HappyNum_11.class, Armstrong_Num_12.class);
				
				System.out.println("######################");
				System.out.println("Total Run Count--> " + result.getRunCount());
				System.out.println("Total Fail Count--> " + result.getFailureCount());
				for (Failure Fail : result.getFailures()) {
					
				System.out.println("Failure Reason-->" + Fail.getMessage().toString());
				
				}
				
				System.out.println("Successful Result->" + result.wasSuccessful());
				
			}
	}

package com.cloudfire.loginmodule;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import com.cloudfire.config.configuration;

public class TC_002_Extract_Emp {

	public static void main(String[] args) {
			
			System.setProperty("webdriver.chrome.driver",System.getProperty("user.dir")+ "\\src\\main\\resources\\Driver\\chromedriver.exe");
			WebDriver driver=new ChromeDriver();
	driver.get(configuration.url);
			driver.manage().timeouts().implicitlyWait(2000, TimeUnit.MILLISECONDS);
			
			WebElement username=driver.findElement(By.name("username"));
			boolean u_name=username.isEnabled();
			
			if(u_name==true)
			{
				//usename entered
				driver.findElement(By.name("username")).sendKeys(configuration.username);
				
			}              
			else
			{
				System.out.println("username invalid");
			}
			
			WebElement passw=driver.findElement(By.name("password"));
			boolean p_word=passw.isEnabled();
			if(p_word==true)
			{
				//password entered
				driver.findElement(By.name("password")).sendKeys(configuration.password);
			}
			else
			{
				System.out.println("password invalid");
			}
			driver.manage().timeouts().implicitlyWait(2000, TimeUnit.MILLISECONDS);
			//login page
			driver.findElement(By.xpath("//button[normalize-space(text()=' Login ')]")).sendKeys(Keys.ENTER);
			
		driver.manage().timeouts().implicitlyWait(2000, TimeUnit.MILLISECONDS);
		driver.findElement(By.xpath("(//ul/li/a)[position()=1]")).click();
		driver.manage().window().maximize();
		
		driver.manage().timeouts().implicitlyWait(2000, TimeUnit.MILLISECONDS);

		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("window.scrollBy(0,1000)");
		
		
		driver.manage().timeouts().implicitlyWait(2000, TimeUnit.MILLISECONDS);

		List<WebElement> list_i=driver.findElements(By.xpath("//div[@role='cell'][position()=3]"));
		List<WebElement> list_in=driver.findElements(By.xpath("//div[@role='cell'][position()=4]"));

		int size1=list_i.size();
        int size2=list_in.size();
		System.out.println("Total Value of User Role is: "+size1);
		System.out.println("Total Employee name value is: "+size2);

		for(int grid=0;grid<size1;grid++)
		{
			System.out.print("User Role--->"+list_i.get(grid).getText());
			System.out.print("****Employee Name --->"+list_in.get(grid).getText());
				System.out.println();

			}
		System.out.println("\n");
		System.out.println("Last 5 Elements of user and Employee");
		System.out.println("************************************");
		for(int i=1;i<=5;i++)
		{
			
			WebElement list_index= driver.findElement(By.xpath("(//div[@role='cell'][position()=4])[last()-"+i+"]"));
			WebElement list_index1= driver.findElement(By.xpath("(//div[@role='cell'][position()=3])[last()-"+i+"]"));
					
			String g=list_index.getText();
			String g1=list_index1.getText();
			System.out.println(g1+"---> "+g);
 
		} 
		
		}

		
}	



package com.cloudfire.loginmodule;

import java.io.File;
import java.time.Duration;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import com.cloudfire.config.configuration;

public class Boss {
public static void main(String[] args) {
		
		System.setProperty("Webdriver.Chrome.driver", 
				System.getProperty("user.dir")+File.separator+"src\\main\\resources\\driver\\chromedriver.exe");
        WebDriver driver = new ChromeDriver();
		
        //Url
		driver.get(configuration.url);
		driver.manage().timeouts().implicitlyWait(3000, TimeUnit.MILLISECONDS);
		driver.manage().window().maximize();
		 
        //UserName
		WebElement user_name =driver.findElement(By.name("username"));
		user_name.clear();
		user_name.sendKeys(configuration.username);
		
        //Password
		WebElement Password = driver.findElement(By.xpath("//*[@name='password']"));
		Password.clear();
		Password.sendKeys(configuration.password);
			
        //Login
	    WebElement Login_btn =driver.findElement(By.xpath("//button[normalize-space(text()=' Login ')]"));
	    Login_btn.click();

	    List<WebElement> grid = driver.findElements(By.tagName("a"));
	    
	    for (int index = 6; index <=6; index++) {
	    	grid.get(index).click();
	}
	    //Scrolling
		WebElement Scroll = driver.findElement(By.partialLinkText("OrangeHRM"));
	    Scroll.sendKeys(Keys.PAGE_DOWN);
	    
	    //Add Button
	    WebElement add_btn = driver.findElement(By.xpath("//div[2]/div[3]/div[1]/div/button"));
	    add_btn.click();
	    
	    //Browse Button
	    WebElement brs_btn = driver.findElement(By.xpath("//input[@type='file']"));
	    brs_btn.sendKeys(System.getProperty("user.dir")+File.separator+"src\\main\\resources\\files\\a.txt");
	    
	    //Save Button
	    WebElement save_btn = driver.findElement(By.xpath("(//button[@type='submit'])[3]"));
	    save_btn.click();
	    
	    driver.manage().timeouts().implicitlyWait(6000, TimeUnit.MILLISECONDS);
	    
	    //Download
	 driver.findElement(By.xpath("//div[@class='oxd-table-card']//button[3]")).click();
	    
	    driver.manage().timeouts().implicitlyWait(3000, TimeUnit.MILLISECONDS);
	    
	    //Delete
	   driver.findElement(By.xpath("//div[@class='oxd-table-card']//button[2]")).click();
	   
	   driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(2));

		driver.findElement(By.xpath("(//div/button)[position()=10]")).click();
	    


	}
}


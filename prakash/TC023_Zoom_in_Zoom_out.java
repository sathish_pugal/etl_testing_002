package com.cloudfire.loginmodule;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class TC023_Zoom_in_Zoom_out {

	public static void main(String[] args) throws InterruptedException {
		// TODO Auto-generated method stub
	
		System.setProperty("windown.chrome.driver", System.getProperty("user.dir")+"\\src\\main\\resources\\Driver\\chromedriver.exe");
		WebDriver driver=new ChromeDriver();
		driver.manage().window().maximize();

		JavascriptExecutor js=(JavascriptExecutor)driver;
		js.executeScript("window.location='https://opensource-demo.orangehrmlive.com/web/index.php/auth/login';");
		js.executeScript("window.scrollTo(0, document.body.scrolllHeight)");
	String ready1=	((String) js.executeScript("return document.readyState",""));
	
	if(ready1.trim().equalsIgnoreCase("Complete"));
	{
		System.out.println("page is loaded Successfully");
	}
	
		Thread.sleep(3000);
		System.out.println("ZOOM IN STARTED");
		
		for(int zoomin=1;zoomin<20;zoomin++)
		{
			driver.findElement(By.tagName("a")).sendKeys(Keys.chord(Keys.CONTROL,Keys.ADD));
			Thread.sleep(1000);
		
		}
		Thread.sleep(10000);
		System.out.println("ZOOM OUT STARTED");
		
		for(int zoomout=1;zoomout<4;zoomout++)
		{
			driver.findElement(By.tagName("a")).sendKeys(Keys.chord(Keys.CONTROL,Keys.SUBTRACT));
		
		}
		System.out.println("DEFAULT SIZE");
		
		driver.findElement(By.tagName("a")).sendKeys(Keys.chord(Keys.CONTROL,Keys.NUMPAD0));
		
		driver.manage().window().fullscreen();

	}

}

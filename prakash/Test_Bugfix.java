package com.cloudfire.loginmodule;

import java.time.Duration;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

import com.cloudfire.config.configuration;
import com.cloudfire.util.Util;

public class Test_Bugfix {

	public static void main(String[] args) throws InterruptedException {
		// TODO Auto-generated method stub
		ChromeOptions opt=new ChromeOptions();
		opt.addArguments("incognito");
		opt.addArguments("start-maximized");
		System.setProperty("webdeiver.chrome.diver", System.getProperty("user.dir")+"\\src\\main\\resources\\Driver\\chromedriver.exe");
		WebDriver driver=new ChromeDriver(opt);
		driver.navigate().to("https://opensource-demo.orangehrmlive.com/");
		driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(4));
		driver.findElement(By.name("username")).sendKeys(configuration.username);
		driver.findElement(By.name("password")).sendKeys(configuration.password);
		driver.findElement(By.xpath("//div/button")).click(); 
		driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(3));
		Util d=new Util();
		d.bug_fix(driver);
	}
}

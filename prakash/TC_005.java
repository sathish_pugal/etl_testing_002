package com.cloudfire.loginmodule;

import java.time.Duration;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import com.cloudfire.config.configuration;

public class TC_005 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir")+"\\src\\main\\resources\\Driver\\chromedriver.exe");
WebDriver driver=new ChromeDriver();
driver.get(configuration.url);
driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(5));
driver.findElement(By.xpath("(//div/p)[3]")).click();
WebElement fp=driver.findElement(By.name("username"));
fp.sendKeys(configuration.username1);
fp.sendKeys(Keys.TAB,Keys.TAB,Keys.ENTER);
driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(8));
String Text_val=driver.findElement(By.xpath("//*[text()='A reset password link has been sent to you via email.']")).getText();
System.out.println(Text_val);
driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(4));

if(Text_val.contains("password"))
{
	System.out.println("Testcase Pass");
}
else
{
	System.out.println("Testcase Fail");
}

	}

}

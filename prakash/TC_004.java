package com.cloudfire.loginmodule;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import com.cloudfire.config.configuration;

public class TC_004 extends configuration {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir")+"\\src\\main\\resources\\Driver\\chromedriver.exe");
		WebDriver driver=new ChromeDriver();
		driver.get(configuration.url);
		driver.manage().timeouts().implicitlyWait(4000, TimeUnit.MILLISECONDS);
	WebElement user_n=	driver.findElement(By.name("username"));
	boolean h=user_n.isEnabled();
	if(h==true)
	{
		driver.manage().timeouts().implicitlyWait(3000, TimeUnit.MILLISECONDS);
		user_n.sendKeys(configuration.username);
	}
	else
	{
		System.out.println("invalid");
	}
	WebElement p_word=driver.findElement(By.name("password"));
	boolean p=p_word.isEnabled();
	if(p==true)
	{
		p_word.sendKeys(configuration.password);
	}
	else
	{
		System.out.println("Invalid");
	}
	driver.findElement(By.cssSelector("button.oxd-button")).sendKeys(Keys.ENTER);
	driver.manage().timeouts().implicitlyWait(4000, TimeUnit.MILLISECONDS);
	String Title_name=driver.getTitle();
	System.out.println("Title of the Page is----->"+ Title_name);
	



	}

}

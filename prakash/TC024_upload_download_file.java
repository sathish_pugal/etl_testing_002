package com.cloudfire.loginmodule;

import java.io.File;
import java.time.Duration;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.edge.EdgeOptions;

import com.cloudfire.config.configuration;

public class TC024_upload_download_file {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		ChromeOptions opt=new ChromeOptions();
		opt.addArguments("incognito");
		opt.addArguments("start-maximized");
		System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir")+"\\src\\main\\resources\\Driver\\chromedriver.exe");
		WebDriver driver=new ChromeDriver(opt);
		//JavascriptExecutor js=(JavascriptExecutor)driver;
		//js.executeScript("window.location='https://opensource-demo.orangehrmlive.com/;");
		driver.navigate().to("https://opensource-demo.orangehrmlive.com/");
		driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(4));
		WebElement user=driver.findElement(By.name("username"));
		user.sendKeys(configuration.username);
		WebElement pwd=driver.findElement(By.name("password"));
		pwd.clear();
		pwd.sendKeys(configuration.password);
		driver.findElement(By.xpath("//div/button")).click(); 
		driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(3));
		List<WebElement> list_anchor=driver.findElements(By.xpath("//ul/li"));
		for(int list=5;list<=5;list++)
		{
			list_anchor.get(list).click();
		}
		driver.findElement(By.linkText("OrangeHRM, Inc")).sendKeys(Keys.PAGE_DOWN);
		
		driver.findElement(By.xpath("//div[@class='oxd-layout-context']/div/div/div/div[2]/div[3]/div/div/button")).click();//Browse
		driver.findElement(By.xpath("//input[@type='file']")).sendKeys(System.getProperty("user.dir")+File.separator+"src\\main\\resources\\files\\a.txt");
		
		driver.findElement(By.xpath("(//div/button[@type='submit'])[position()=3]")).click();//save button

		
		driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(6));

		WebElement f=driver.findElement(By.xpath("//div[@class='oxd-table-card']/div/div[8]/div/button[3]"));
		f.click();//download button
		
		driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(2));

		driver.findElement(By.xpath("//div[@class='oxd-table-card']/div/div[8]/div/button[2]")).click();//delete button
		 //delete confirmation
		
	}

}

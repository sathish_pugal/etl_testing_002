package com.cloudfire.loginmodule;

import java.time.Duration;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class TC009_radio_button_validation {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir")+"\\src\\main\\resources\\Driver\\chromedriver.exe");
WebDriver driver=new ChromeDriver();
driver.navigate().to("https://demoqa.com/automation-practice-form");
driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(4));
driver.manage().window().maximize();

int hei=driver.manage().window().getSize().getHeight();
int wid=driver.manage().window().getSize().getWidth();
System.out.println(hei+" "+wid);
driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(10));

JavascriptExecutor js = (JavascriptExecutor) driver;
js.executeScript("window.scrollBy(0,100 0)", "");
WebElement radio_button=driver.findElement(By.xpath("//input[@id='gender-radio-1']//..//label"));
boolean radi_btn=radio_button.isEnabled();
if(radi_btn==true)
{
	radio_button.click();
}
for(int i=1;i<=3;i++)
{
	WebElement ra_tn=driver.findElement(By.xpath("(//*[@type='radio']//..//label)["+i+"]"));
	if(ra_tn.isEnabled()==true)
	{
	ra_tn.click();
	}
}
}
}

package com.cloudfire.loginmodule;

import java.time.Duration;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

public class TC014_Dnamic_web_table {
	public static void main(String[] args) {
		// TODO Auto-generated method stub
ChromeOptions opt=new ChromeOptions();
opt.addArguments("-incognito");
opt.addArguments("--start-maximized");
System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir")+"\\src\\main\\resources\\Driver\\chromedriver.exe");
WebDriver driver=new ChromeDriver();
driver.navigate().to("https://money.rediff.com/gainers/bse/daily/groupa?src=gain_lose");
driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(3));
double curr_price=0;
List<WebElement> gainer_header=driver.findElements(By.xpath( "(//div/h2)[1]//../div/a"));
List<WebElement> row_li=driver.findElements(By.xpath("//table[@class='dataTable']//tbody/tr"));
List<WebElement> Col_li=driver.findElements(By.xpath("//table[@class='dataTable']//thead/tr/th"));
for(int i=1;i<=gainer_header.size();i++)
{
	WebElement value=driver.findElement(By.xpath("((//div/h2)[1]//../div/a)["+i+"]"));
	value.click();
	for(int row=1;row<=row_li.size();row++)
	{ 
		for(int col=Col_li.size()-1;col<=Col_li.size()-1;col++)
		{

			WebElement price=driver.findElement(By.xpath("//table[@class='dataTable']//tbody/tr["+row+"]/td["+col+"]"));
			curr_price=curr_price+ Double.parseDouble(price.getText().trim().replace(",", ""));
		}
	}
	
}  
System.out.println("************************************************************");
System.out.println("Value of Gainer Price Daily & Weekly, Monthly is: \n");
System.out.println(+curr_price);
}
}
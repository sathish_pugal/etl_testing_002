package com.cloudfire.loginmodule;

import java.time.Duration;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.edge.EdgeOptions;

import com.cloudfire.config.configuration;
import com.cloudfire.util.Util;
import com.cloudfire.util.Util_zoom;

public class TC025_Zoom_in_out {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		EdgeOptions opt=new EdgeOptions();
		opt.addArguments("-incognito");
		opt.addArguments("--start-maximized");
		System.setProperty("webdriver.edge.driver", System.getProperty("user.dir")+"\\src\\main\\resources\\Driver\\msedgedriver.exe");
		WebDriver driver=new EdgeDriver(opt);
		//JavascriptExecutor js=(JavascriptExecutor)driver;
		//js.executeScript("window.location='https://opensource-demo.orangehrmlive.com/;");
		driver.navigate().to("https://demoqa.com/");
		driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(4));
		/*WebElement user=driver.findElement(By.name("username"));
		user.clear();
		user.sendKeys(configuration.username);
		WebElement pwd=driver.findElement(By.name("password"));
		pwd.clear();
		pwd.sendKeys(configuration.password);
		driver.findElement(By.xpath("//div/button")).click(); 
		driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(3));*/

		Util_zoom.ZoomOut();
		try
		{
			Thread.sleep(4000);
		}catch(Exception e)
		{
			e.printStackTrace();
		}


		driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(8));
		
		Util_zoom.ZoomIn();
		try
		{
			Thread.sleep(4000);
		}catch(Exception e)
		{
			e.printStackTrace();
		}
		
	}

}

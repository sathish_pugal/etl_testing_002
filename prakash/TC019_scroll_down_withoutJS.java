package com.cloudfire.loginmodule;

import java.time.Duration;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

public class TC019_scroll_down_withoutJS {

	public static void main(String[] args) throws InterruptedException {
		// TODO Auto-generated method stub
		ChromeOptions s = new ChromeOptions();
		s.addArguments("-incognito");
		s.addArguments("start-maximizied");

		System.setProperty("webdriver.chrome.driver",
				System.getProperty("user.dir") + "\\src\\main\\resources\\Driver\\chromedriver.exe");
		WebDriver driver = new ChromeDriver(s);
		driver.navigate().to("https://shop.demoqa.com/my-account/");
		driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(3));
		int he = driver.manage().window().getSize().getHeight();
		int wid = driver.manage().window().getSize().getWidth();
		System.out.println("maximum Height and Width: " + he + "&" + wid);// 708 && 1050

		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("window.scrollBy(0,354);", "");
		Thread.sleep(8000);
		js.executeScript("window.scrollBy(0,-300);", "");
		Thread.sleep(8000);
		js.executeScript("window.scrollTo(0,300)", "");
		
		

		
		  driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(3));
		  
		 WebElement scoll_view=driver.findElement(By.cssSelector("div.noo-bottom-bar-content>div>:nth-child(1)"));
		 // js.executeScript("arguments[arguments.length-1].scrollIntoView(true)", scoll_view);
		  js.executeScript("window.scrollBy(0,document.body.scrollHeight)");
		  
		  for(int i=0;i<=4;i++) 
		  {
			  scoll_view.sendKeys(Keys.PAGE_UP);
		  }
		 

	}

}

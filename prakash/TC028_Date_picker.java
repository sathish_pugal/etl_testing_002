package com.cloudfire.loginmodule;

import java.io.File;
import java.io.IOException;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import com.cloudfire.config.configuration;
import com.cloudfire.util.Util;
import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

public class TC028_Date_picker {

	public static ExtentReports extent;
	public static ExtentTest test;
	
	public static void main(String[] args) throws IOException {
		// TODO Auto-generated method stub
		extent=new ExtentReports(System.getProperty("user.dir")+File.separator+"src\\main\\resources\\Reports\\DataReport.html",false);
		extent.loadConfig(new File(System.getProperty("user.dir")+File.separator+"src\\main\\resources\\config\\extent_config.xml"));
		test=extent.startTest("Text Start Successfully");
		test.assignAuthor("PRAKASH");
		test.assignCategory("Smoke-Testing-->Production");
		ChromeOptions option=new ChromeOptions();
		option.addArguments("-incognito");
		option.addArguments("start-maximized");
		System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir")+File.separator+"\\src\\main\\resources\\Driver\\chromedriver.exe");
		WebDriver driver=new ChromeDriver(option);
		Util.Environment_info(driver);
		JavascriptExecutor js=(JavascriptExecutor)driver;
		//js.executeScript("Window.location='https://only-testing-blog.blogspot.com/2014/09/selectable.html'");
		driver.navigate().to(configuration.url1);
		test.log(LogStatus.INFO,"Browser Launched Successfully");
		String exepect_url="https://only-testing-blog.blogspot.com/2014/09/selectable.html";
		String actual_url=driver.getCurrentUrl();
		test.log(LogStatus.INFO, "URL info:: "+actual_url);
		if(actual_url.equals(exepect_url))
		{
			test.log(LogStatus.PASS, "Actual URL::: "+actual_url);
		}
		else
		{
			test.log(LogStatus.FAIL, "Expect URL:::"+exepect_url);
		}
		
		boolean q=Util.Check_page_ready(driver);
		if(q)
		{
		WebElement p=driver.findElement(By.xpath("//input[@id='datepicker'or @type='text']"));
		if(p.isEnabled())
		{
			test.log(LogStatus.PASS, "element is enabled");
		}
		else
		{
			test.log(LogStatus.FAIL,"Element is snot enabled");
		}
		
		p.click();
		Util.Date_picking("11", driver);
		test.log(LogStatus.PASS, "Page is Loaded");
		}
		else
		{
			test.log(LogStatus.FAIL, "Page is not Loaded");
		}
		
		Util.Screen_shot(driver);
		test.log(LogStatus.INFO, "Screen Captured Successfully");
		
		extent.endTest(test);
		extent.flush();
		extent.close();
		
		

	}

}

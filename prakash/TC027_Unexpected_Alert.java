package com.cloudfire.loginmodule;

import java.io.File;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.devtools.idealized.Javascript;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.edge.EdgeOptions;

import com.cloudfire.config.configuration;
import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

public class TC027_Unexpected_Alert {
	public static ExtentReports extent;
	public static ExtentTest test;
	
	public static void main(String[] args) throws InterruptedException {
	
		extent=new ExtentReports(System.getProperty("user.dir")+File.separator+"src\\main\\resources\\Reports\\Myreports.html",true);
		extent.loadConfig(new File(System.getProperty("user.dir")+File.separator+"src\\main\\resources\\config\\extent_config.xml"));
		test=extent.startTest("Browser Launched Successfully");
		test.assignAuthor("PRAKASH");
		test.assignCategory("Smoke Testing-Prod");
		System.setProperty("webdriver.edge.driver", System.getProperty("user.dir")+configuration.path+File.separator+"msedgedriver.exe");
		WebDriver driver=new EdgeDriver();
		JavascriptExecutor js=(JavascriptExecutor)driver;

		driver.navigate().to(configuration.url1);
		test.log(LogStatus.PASS, "Browser Successfully Loaded");
		/*
		 * JavascriptExecutor js=(JavascriptExecutor)driver; js.executeScript(
		 * "window.location='https://only-testing-blog.blogspot.com/2014/06/alert_6.html"
		 * ); 
		 * */
		
		String exepect_url="https://only-testing-blog.blogspot.com/2014/01/textbox1.html";
		String actual_url=driver.getCurrentUrl();
		test.log(LogStatus.INFO, "URL info:: "+actual_url);
		if(actual_url.equals(exepect_url))
		{
			test.log(LogStatus.PASS, "Actual URL::: "+actual_url);
		}
		else
		{
			test.log(LogStatus.FAIL, "Expect URL:::"+exepect_url);
		}
		WebElement d=driver.findElement(By.partialLinkText("Older Post"));
		js.executeScript("arguments[0].scrollIntoView(true);",d);
		driver.findElement(By.cssSelector("input[value='Show Me Alert']")).click();
		
		Alert a=driver.switchTo().alert();
		String txt_show=a.getText();
		test.log(LogStatus.INFO, "Tex is:"+txt_show);
		a.dismiss();
		
		driver.manage().window().fullscreen();
		test.log(LogStatus.PASS, "Browser getting Maximized");
		
		driver.findElement(By.cssSelector("button[onclick='myFunction()']")).click();

		Alert a1=driver.switchTo().alert();
		String txt_show1=a1.getText();
		
		a1.dismiss();
/*Alert a1=driver.switchTo().alert();

a1.accept();*/

extent.endTest(test);
//extent.flush();
extent.close();

	}

}

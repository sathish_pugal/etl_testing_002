package com.cloudfire.loginmodule;

import java.time.Duration;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class Tooltip_validation {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir")+"\\src\\main\\resources\\Driver\\chromedriver.exe");
WebDriver driver=null;
driver=new ChromeDriver();
driver.navigate().to("https://www.google.co.in/");
driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(5));
String Expect_text="search";
WebElement tool_tip=driver.findElement(By.xpath("//input[@type='text']"));
String Actual_text=tool_tip.getAttribute("title").trim();

tool_tip.sendKeys("Messi Goal against Australia");

driver.findElement(By.xpath("(//input[@type='submit'])[3]")).click();

if(Actual_text.equalsIgnoreCase(Expect_text)==true)
{
	System.out.println("Test Case Pass");
}
else
{
	System.out.println("Testcase fail");
}
	}

}

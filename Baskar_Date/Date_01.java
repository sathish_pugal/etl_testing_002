package com.cloudfire.Date_Calendar;

import java.text.SimpleDateFormat;
import java.util.Date;

public class Date_01 {

	public static void main(String[] args) {

		SimpleDateFormat sd = new SimpleDateFormat("dd:MM:yyyy HH:mm:ss:SS");
		String current_date = sd.format(new Date());
		System.out.println("Today Date is: " + current_date);

	}

}
